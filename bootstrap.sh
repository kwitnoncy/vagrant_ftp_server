# Update Packages
apt-get update
# Upgrade Packages
apt-get upgrade

# Basic Linux Stuff
apt-get install -y git

#   TS -> laboratory class (FTP)
apt-get install -y mc
apt-get install -y vsftpd
apt-get install update

sudo echo "" >> /etc/vsftpd.conf
sudo echo "local_enable=YES" >> /etc/vsftpd.conf
sudo echo "" >> /etc/vsftpd.conf
sudo echo "write_enable=YES" >> /etc/vsftpd.conf
sudo echo "" >> /etc/vsftpd.conf

sudo service vsftpd restart

sudo adduser piotr
sudo echo "piotr:kwiat" | chpasswd
